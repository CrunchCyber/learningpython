import time
import threading
import concurrent.futures
start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(1)
    return f'Done sleeping...{seconds} second(s)'


# threads = []
# for _ in range(10):
#     t = threading.Thread(target=do_something, args=[1.5])
#     t.start()
#     threads.append(t)
#
# for t in threads:
#     t.join()


with concurrent.futures.ThreadPoolExecutor() as executor:
    secs = [5, 4, 3, 2, 1]
    results = executor.map(do_something, secs)

    for result in results:
        print(result)


finish = time.perf_counter()

print(f'Finished in {round(finish-start,2)} second(s)')
