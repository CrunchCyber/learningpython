import os
import time
from multiprocessing import Process, current_process


def square(numbers):
    for number in numbers:
        result = number*number
        time.sleep(0.5)
        print(f'The number {number} squares to {result}')





if __name__ == '__main__':

    processes = []
    numbers = range(100)

    for i in range(50):
        process = Process(target=square, args=(numbers,))
        processes.append(process)

        process.start()

    for process in processes:
        process.join()